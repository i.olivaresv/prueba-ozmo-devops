## Paso 1

Descargar Carpeta HTML

## Paso 2

Entrar a carpeta html

cd html

## Paso 3

Ejecutar build de la imagen Docker

docker build -t html .

## Paso 4

Ejecutar docker run para levantar el server

docker run -p 8080:80 -d html

## Paso 5

Validar html en el siguiente enlace

http://0.0.0.0:8080/test/. o http://localhost:8080/test/

## Validación 

curl -sL http://localhost:8080/test/ | base64 -d 

